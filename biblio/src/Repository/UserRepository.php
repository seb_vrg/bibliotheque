<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

   public function searchUser(array $filters = [])
   {
       $qb = $this->createQueryBuilder('u');
       $qb->orderBy('u.lastname','ASC');

       if (!empty($filters['lastname'])) {
           $qb->andWhere('u.lastname LIKE :lastname')
               ->setParameter(':lastname', '%' .$filters['lastname']. '%');
       }
       if (!empty($filters['firstname'])) {
           $qb->andWhere('u.firstname LIKE :firstname')
               ->setParameter(':firstname', '%' .$filters['firstname']. '%');
       }


       return $qb;
   }
}
