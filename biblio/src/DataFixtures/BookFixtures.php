<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class BookFixtures extends Fixture
{
    const COUNT = 200;
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0 ; $i < self::COUNT; $i++)
        {
            $book = new Book();
            $book->setAuthor($faker->userName);
            $book->setTitle($faker->sentence);
            $book->setDescription($faker->text());
            $manager->persist($book);

        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */

}
