<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LoanRepository")
 */
class Loan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startdate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $enddate;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $restitutionprev;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="loans")
     * @Assert\NotBlank(message="Adhérent obligatoire !")
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Book", inversedBy="loans")
     *
     */
    private $Book;

    public function __construct()
    {
        $this->startdate = new \DateTime('NOW');
        $today =  new \DateTime('NOW');
        $delai = new \DateInterval('P1M');
//
       $this->restitutionprev = $today->add($delai);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartdate(): ?\DateTimeInterface
    {
        return $this->startdate;
    }

    public function setStartdate(\DateTimeInterface $startdate): self
    {
        $this->startdate = $startdate;

        return $this;
    }

    public function getEnddate(): ?\DateTimeInterface
    {
        return $this->enddate;
    }

    public function setEnddate(?\DateTimeInterface $enddate): self
    {
        $this->enddate = $enddate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->Book;
    }

    public function setBook(?Book $Book): self
    {
        $this->Book = $Book;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRestitutionprev(): \DateTime
    {
        return $this->restitutionprev;
    }

    /**
     * @param \DateTime $restitutionprev
     * @return Loan
     */
    public function setRestitutionprev(\DateTime $restitutionprev): Loan
    {
        $this->restitutionprev = $restitutionprev;
        return $this;
    }

}
