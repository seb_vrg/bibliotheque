<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Loan;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


            ->add('User',EntityType::class,[
                'class' => User::class,
                'label' => 'Adhérents :',
                'placeholder' => 'Séléctionnez un adhérent'

            ])
//            ->add('Book',EntityType::class,[
//                'class' => Book::class,
//                'label' => 'Livres : ',
//                'placeholder' => 'Séléctionnez un Livre :'
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Loan::class,
        ]);
    }
}
