<?php

namespace App\Form;

use App\Entity\Administrator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdministratorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname',TextType::class,[
                'label' => 'Nom',
                'required' => true
            ])
            ->add('firstname',TextType::class,[
                'label' => 'Prénom',
                'required' => true
            ])
            ->add('email',EmailType::class,[
                'label' => 'Email',
                'required' => true
            ])
           ->add('plainPassword',RepeatedType::class,[
               'type' => PasswordType::class,
               'first_options' => [
                   'label' => 'Mot de passe'
               ],
               'second_options' => [
                   'label' => 'Répétez le mot de passe'
               ],
               'invalid_message' => 'Les mots de passe sont diférents'
           ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Administrator::class,
        ]);
    }
}
