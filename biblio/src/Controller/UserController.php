<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Entity\User;
use App\Form\SearchUserType;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;



/**
 * @IsGranted("ROLE_USER")
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{
    /**
     * @Route("/user/{page}", requirements={"page" : "\d+"},defaults={"page" : 1},name="user")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Response
     * @param $page
     */
    public function index(Request $request,UserRepository $userRepository,$page)
    {
        $formSearch = $this->createForm(SearchUserType::class);
        $formSearch->handleRequest($request);

        $listUsers = $userRepository->searchUser((array)($formSearch->getData()));

        $adapter = new DoctrineORMAdapter($listUsers);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage($this->getParameter('nb_user_per_page'));
        $pager->setCurrentPage((int)$page);

        return $this->render('user/index.html.twig', [
            'pager' => $pager,
            'page' => $page,
            'search' => $formSearch->createView()
        ]);
    }

    /**
     * @Route("/datatableList",name="listdatatable")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return Response
     */
    public function indexDataTable(Request $request,UserRepository $userRepository)
    {
        $formSearch = $this->createForm(SearchUserType::class);
        $formSearch->handleRequest($request);

        $listUsers = $userRepository->searchUser((array)($formSearch->getData()));

       return $this->render('user/list_data_table.html.twig',[
            'list' => $listUsers
       ]);
        }

    /**
     * @Route("/editUser/{id}",name="edituser",defaults={"id": null},requirements={"id" : "\d+"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param $id
     * @return Response
     */
    public function editUser(Request $request, EntityManagerInterface $manager,$id)
    {

        if (is_null($id)) {
            $user = new User();
            $type = "Ajouter";
        } else {
            $user = $manager->find(User::class,$id);
            $type ="Modifier";

            if (is_null($user)) {
                throw new NotFoundHttpException();
            }

        }


        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $firstNameUser = $user->getFirstname();
                $lastNameUser = $user->getLastname();

                $manager->persist($user);
                $manager->flush();

                $this->addFlash('success',"<p class='text-center'>Le nouvel adhérent <strong>$firstNameUser $lastNameUser</strong> est enregistré.</p>");

                return $this->redirectToRoute('user');
            }
        }


        return $this->render('user/edit_user.html.twig',[
            'form' => $form->createView(),

            'type' => $type
        ]);
    }

    /**
     * @Route("/deleteUser/{id}",name="deleteuser")
     * @param User $user
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function deleteUser(User $user,EntityManagerInterface $manager)
    {

        $lastNameUser = $user->getLastname();
        $firstNameUser = $user->getFirstname();

        $loans = $user->getLoans();
        $nbLoans = count($loans);

        if ( $nbLoans > 0 ) {
           $this->addFlash('warning',"<p class='text-center'>Suppression impossible, <strong>$firstNameUser $lastNameUser</strong> à un ou des emprunt(s) en cours </p>");

           return $this->redirectToRoute('listloans',['id' => $user->getId()]);
        } else {
            $manager->remove($user);
            $manager->flush();
            $this->addFlash('success',"<p class='text-center'><strong>$firstNameUser $lastNameUser</strong> n'est plus adhérent.</p>");

            return $this->redirectToRoute('user');
        }


    }

    /**
     * @Route("/listLoans/{id}",name="listloans")
     * @param User $user
     * @return Response
     */
    public function listLoansForUser(User $user)
    {

        $loans = $user->getLoans();

        return $this->render('user/list_loan.html.twig',[
            'loans' => $loans,
            'user' => $user
        ]);
    }

}
