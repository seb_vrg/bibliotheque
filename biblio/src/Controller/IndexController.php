<?php


namespace App\Controller;


use App\Entity\Administrator;
use App\Form\AdministratorType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{

    /**
     * @Route("/",name="home")
     */
    public function home()
    {

        return $this->render('index/index.html.twig');
    }

    /**
     * @Route("/inscription",name="register")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function register(Request $request,UserPasswordEncoderInterface $passwordEncoder,EntityManagerInterface $manager)
    {
        $bibliothecaire = new Administrator();

        $form = $this->createForm(AdministratorType::class,$bibliothecaire);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $password = $passwordEncoder->encodePassword(
                    $bibliothecaire, $bibliothecaire->getPlainPassword()
                );
                $lastNameBiblio = $bibliothecaire->getLastname();
                $firstNameBiblio = $bibliothecaire->getFirstname();
                $bibliothecaire->setPassword($password);
                $manager->persist($bibliothecaire);
                $manager->flush();

                $this->addFlash('success',"<p class='text-center'><strong>$firstNameBiblio $lastNameBiblio</strong> est enregistré comme bibliothécaire</p>");
                return $this->redirectToRoute('home');
            } else {
                $this->addFlash('error',"<p class='text-center'>Le formulaire contient des erreurs</p>");
            }
        }

        return $this->render('index/register.html.twig',[
            'form'=> $form->createView()
        ]);

    }

    /**
     * @Route("/connexion", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        if (!empty($error)) {
            $this->addFlash('error','Identifiants incorrects');
        }

        return $this->render('index/login.html.twig',[
            'last_username' => $lastUsername
        ]);
    }

    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logout(){}
}