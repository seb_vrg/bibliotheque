<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Form\SearchBookType;
use App\Repository\BookRepository;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @IsGranted("ROLE_USER")
 * Class BookController
 * @package App\Controller
 */
class BookController extends AbstractController
{
    /**
     * @Route("/book/{page}", requirements={"page" : "\d+"},defaults={"page" : 1} ,name="books")
     * @param Request $request
     * @param BookRepository $books
     * @param $page
     * @return Response
     */
    public function index(Request $request,BookRepository $books,$page)
    {
        $searchForm = $this->createForm(SearchBookType::class);
        $searchForm->handleRequest($request);


        $list = $books->searchBook((array)($searchForm->getData()));

        $adapter = new DoctrineORMAdapter($list);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage($this->getParameter("nb_books_per_page"));
        $pager->setCurrentPage((int)$page);

        return $this->render('book/index.html.twig', [
            'pager' => $pager,
            'form_search' =>$searchForm->createView(),
            'page' => $page
        ]);
    }

    /**
     * @Route("/whoRentBook/{id}",name="whereisbook")
     * @param Book $book
     * @param LoanRepository $loanRepository
     * @return Response
     */
    public function whoRentBook(Book $book,LoanRepository $loanRepository)
    {
        $title = $book->getTitle();
        $author = $book->getAuthor();



        $infoLoan = $loanRepository->findOneBy([
            'Book' => $book
        ],[]);

//        dump($infoLoan);
//        die();

        return $this->render('book/who_rent_book.html.twig',[
            'loan' =>$infoLoan,
            'book' => $book
        ]);
    }

    /**
     * @Route("/editBook/{id}",defaults={"id": null}, requirements={"id" : "\d+"},name="editbook")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param $id
     * @return Response
     */
    public function editBook(Request $request,  EntityManagerInterface $manager,$id)
    {
        if (is_null($id)) {
            $book = new Book();
            $type = "Ajouter";
        } else {
            $book = $manager->find(Book::class,$id);
            $type = "Modifier";

            if (is_null($book)) {
                throw new NotFoundHttpException();
            }
        }


        $form = $this->createForm(BookType::class,$book);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $title = $book->getTitle();
                $author = $book->getAuthor();

                $manager->persist($book);
                $manager->flush();

                $this->addFlash('success',"<p class='text-center'> Le livre <cite>$title</cite> de <strong>$author</strong> a bien été ajouté</p>");

                return $this->redirectToRoute('books');

            } else {
                $this->addFlash('error',"Le formulaire contient des erreurs");
            }

        }


        return $this->render('book/edit_book.html.twig',[
            'form' => $form->createView(),
            'type' => $type
        ]);
    }

    /**
     * @Route("deletebook/{id}", name="deletebook")
     * @param Book $book
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function deleteBook(Book $book,EntityManagerInterface $manager)
    {
        $title = $book->getTitle();
        $author = $book->getAuthor();

        if (!$book->getLoans()->isEmpty()) {

            $this->addFlash('error',"<p class='text-center'><strong>Impossible de supprimer le livre </strong><cite>$title</cite>, il est déjà emprunté !</p>");
        } else {
            $manager->remove($book);
            $manager->flush();
            $this->addFlash('success',"Le livre <cite>$title</cite> de <strong>$author</strong> à bien été supprimé");
        }

        return $this->redirectToRoute('books');
    }


}
