<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Form\ContactUserType;
use App\Form\LoanType;
use App\Repository\BookRepository;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\DataCollector\DumpDataCollector;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @IsGranted("ROLE_USER")
 * Class LoanController
 * @package App\Controller
 */
class LoanController extends AbstractController
{
    /**
     * @Route("/loan", name="loan")
     * @param LoanRepository $loanRepository
     * @return Response
     * @throws \Exception
     */
    public function index(LoanRepository $loanRepository)
    {
            $date = new \DateTime('NOW');

           // $dateToday = $date->format('d/m/Y');



            $listEmprunts =  $loanRepository->findBy([],[
                'startdate' => 'ASC'
            ]);


        return $this->render('loan/index.html.twig', [
            'list' => $listEmprunts,
            'date' => $date
        ]);
    }

    /**
     * @Route("/editLoan", name="editloan")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param BookRepository $bookRepository
     * @return Response
     * @throws \Exception
     */
    public function editLoan(Request $request,EntityManagerInterface $manager,BookRepository $bookRepository)
    {


        $emprunt = new Loan();

        $date = new \DateTime('NOW');
        $empruntsList = $this->createForm(LoanType::class,$emprunt);


        $bookDispos = $bookRepository->findBy([
            'isRent' => false

        ],[]);



        $empruntsList->handleRequest($request);

        if ($empruntsList->isSubmitted()) {

            if ($empruntsList->isValid()) {

                $bookId = $request->request->get('choicebook');

                if (!empty($bookId)) {

                    $bookChoice = $bookRepository->find($bookId);
                    $emprunt->setBook($bookChoice);

                    $bookChoice->setIsRent(true);
                    $titleBook = $bookChoice->getTitle();
                    $userRent = $emprunt->getUser();
                    $manager->persist($emprunt);
                    $manager->flush();

                  $this->addFlash('success',"<p class='text-center'> Le livre <strong>$titleBook</strong> à été emprunté par <strong>$userRent</strong></p>");
                    return $this->redirectToRoute('loan');


                }

            } else {
                $this->addFlash('error',"<p class='text-center'>Le formulaire contient des erreurs !</p>");
            }
        } else {
            $test = null;
            $isRent = null;
        }

        return $this->render('loan/edit_loan.html.twig',[
            'form' =>$empruntsList->createView(),
            'date' => $date,

            'listBook' => $bookDispos
        ]);
    }

    /**
     * @Route("/restitution/{id}",name="restitute")
     * @param Loan $loan
     * @param BookRepository $bookRepository
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function getRestitution(Loan $loan,BookRepository $bookRepository,EntityManagerInterface $manager)
    {
        $restituedBookId = $loan->getBook()->getId();
        $bookAvailable =$bookRepository->find($restituedBookId);
        $bookTitle = $bookAvailable->getTitle();
        $bookAuthor = $bookAvailable->getAuthor();
        $firstNameUserRent = $loan->getUser()->getFirstname();
        $lastNameUserRent = $loan->getUser()->getLastname();

        $bookAvailable->setIsRent(false);

        $manager->remove($loan);
        $manager->flush();

        if (is_null($loan->getId())) {
            $this->addFlash('success',"<p class='text-center'>Le livre <strong>$bookTitle</strong> de <strong>$bookAuthor</strong> a été restitué par <strong> $firstNameUserRent $lastNameUserRent</strong></p>");

        } else {
            $this->addFlash('error',"<p class='text-center'><strong>Une erreur est survenue lors de la restitution</strong></p>");
        }
        return $this->redirectToRoute('loan');

    }

    /**
     * @Route("/contactUser/{id}",name="contactuser")
     * @param Request $request
     * @param Loan $loan
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function sendMailToAdherant(Request $request,Loan $loan,\Swift_Mailer $mailer)
    {
        $firstnameUser = $loan->getUser()->getFirstname();
        $lastnameUser = $loan->getUser()->getLastname();
        $mailUser = $loan->getUser()->getEmail();
        $bookTitle = $loan->getBook()->getTitle();
        $bookAuthor = $loan->getBook()->getAuthor();
        $prevDateRestitution = $loan->getRestitutionprev()->format('d/m/Y');
        $loanDate = $loan->getStartdate()->format('d/m/Y');

        $subjectMail = "RETARD DE RESTITUTION DE LIVRE A LA BIBLIOTHEQUE";
        $contentMail = "A l'attention de $firstnameUser $lastnameUser,\n";
        $contentMail .= "Le livre $bookTitle de $bookAuthor";
        $contentMail .= " que vous avez emprunté le $loanDate est toujours en votre possession.\n";
        $contentMail .= "Ce dernier aurait dû être réstitué au plus tard le $prevDateRestitution !\n ";
        $contentMail .= "Merci de le rapporter immédiatement, sous peine de poursuites.\n";
        $contentMail .= "Cordialement,\n ";
        $contentMail .= "La direction.";

        $form = $this->createForm(ContactUserType::class);
        $form->get('email')->setData($mailUser);
        $form->get('subject')->setData($subjectMail);
        $form->get('content')->setData($contentMail);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $data = $form->getData();
                $expeditor = $this->getParameter('contact_email');

                $mail = $mailer->createMessage();
                $mailBody = $this->renderView('loan/contact_body.html.twig',[
                    'datas' => $data
                ]);

                $mail->setSubject($subjectMail)
                     ->setFrom($expeditor)
                     ->setTo($mailUser)
                     ->setBody($mailBody,'text/html')
                     ->setReplyTo($expeditor)
                ;

                $mailer->send($mail);
                $this->addFlash('success',"<p class='text-center'>Le message de relance a bien été envoyé à $mailUser</p>");
                    return $this->render('loan/view_message_sent.html.twig',[
                        'datas' => $data
                    ]);


            } else {
                $this->addFlash('error',"Le formulaire contient des erreurs");
            }
        }


        return $this->render('loan/contact_user.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
